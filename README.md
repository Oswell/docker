## 项目介绍


> [houdunren.com](https://www.houdunren.com) @ 向军大叔


本项目是高可用的LNMP开发环境，同时支持代理转发，实现单台服务器运行多个DOCKER网站。

> 本项目为视频代码或直播课程代码，希望大家能给个 `star` 这是对我们最好的鼓励和肯定。

![img](./assets/1*d22uLmzoYTq24BW3PfJfyA.png)

**项目特点**

1. 保持使用较新版本的NGINX/PHP/MYSQL/REDIS环境套件
2. 容器参数可自定义配置
3. 如有问题请在[后盾人](https://www.houdunren.com)反馈，会得到及时处理

### 获取帮助

[后盾人](https://www.houdunren.com) 已经发布了DOCKER相关视频教程，可以帮助你掌握DOCKER技术。

Github 仓库：https://github.com/houdunwang/docker

Gitee 仓库：https://gitee.com/houdunren/docker

## 安装配置

### 防火墙

为了在本地测试成功可以先关闭防火墙

在学习阶段可以先关闭防火墙，保证端口不受访问限制，以下是LINUX管理防火墙的基本指令 

```
sudo systemctl stop firewalld.service 
```

练习时也可以永久关闭防火墙

```
sudo systemctl disable firewalld.service
```

查看防火墙状态

```
sudo systemctl status firewalld.service
```

然后重起 DOCKER

```
sudo systemctl restart docker
```

关闭setlinux

 1. 修改配置文件

    ```
    sudo vim /etc/selinux/config
    ```

	2. 修改 `SELINUX` 值为disabled

    ```
    SELINUX=disabled
    ```

### 下载项目

从 **GITHUB** 或 **GITEE** 下CLONE项目代码

```
git clone https://gitee.com/houdunren/docker.git
cd docker
```

### 目录说明

下面是实验的文件结构，便于有个全局认识

```
.
├── docker-compose.yaml	  容器编排
├── mysql									MYSQL容器
│   ├── Dockerfile				镜像配置
│   ├── data							数据结构
│   └── log								运行日志
├── nginx						
│   ├── Dockerfile
│   ├── config
│   │   └── default.conf	NGINX配置
│   └── log								运行日志
│       ├── access.log
│       └── error.log
├── redis						
│   ├── Dockerfile
│   ├── config
│   │   └── redis.conf	  REDIS配置
│   └── log								运行日志
│       ├── access.log
│       └── error.log
└── php
│    ├── Dockerfile
│    └── config
│        └── php.ini			PHP配置文件
└── www										应用目录
    ├── hdcms
    │   ├── index.html
    │   └── index.php
    └── houdunren
        ├── index.html
        └── index.php
```

### 配置参数

通过修改 **.env** 可以对容器的参数进行定制，比如数据库帐号密码等

```
#容器名前缀
CONTAINER_NAME_PRE=houdunren

#宿主机映射到容器的端口
NGINX_PORT=80

#宿主机映射到MYSQL容器的端口
MYSQL_PORT=33060
#ROOT管理员密码
MYSQL_ROOT_PASSWORD=admin888

#新建数据库
MYSQL_DATABASE=houdunren

#新建普通用户帐号密码
MYSQL_USER=houdunren
MYSQL_PASSWORD=houdunren

#MYSQL初始后强制ROOT帐号改密码
MYSQL_ONETIME_PASSWORD=no

#允许MYSQL空密码
MYSQL_ALLOW_EMPTY_PASSWORD
```

### 项目配置

系统包括NGINX、PHP等软件的项目配置文件，修改这些配置文件不需要重新编译，只需要在`docker-compose.yaml`文件所在目录下重起容器就可以了。

```
docker-compose restart
```

### 编译执行

执行以下命令将编译镜像并启动容器

```
docker-compose up -d
```

查看容器

```
docker ps
```



## 访问测试 

如果在本地测试时，修改 `/etc/hosts`文件中添加两个域名，线上使用将域名解析过来就可以 了

```
127.0.0.1	hdcms.test
127.0.0.1	houdunren.test
```

现在可以通过 `http://houdunren.test` 访问项目了

![image-20200112122147320](./assets/image-20200112122147320.png)

## LARAVEL

下面来安装LARAVEL项目，你可以安装任何其它PHP项目来使用，具体可以查看[后盾人](https://www.houdunren.com)在线文档或视频学习LARAVEL的安装使用。

```
cd www
rm *
laravel new .
```

因为LARAVEL要解析到**public**目录，修改NGINX配置文件 `nginx/config/default.conf` 目录相关内容

```
location / {
	...
  root   /www/houdunren/public; 
  ...
}
...
location ~ \.php$ {
  ...
  fastcgi_param  SCRIPT_FILENAME  /www/houdunren/public$fastcgi_script_name;
  ...
}
```

修改配置后需要生起容器服务

```
docker-compose restart
```

现在访问就可以看到LARAVEL欢迎页面了

![image-20200112124436521](./assets/image-20200112124436521.png)

## 数据库

下面我们使用MYSQL管理GUI工具DBeaver连接容器数据库，默认MYSQL端口是33060可以在.env文件中修改。

如果修改了.env 中的配置需要重新编译容器

```
docker-compose up -d
```



使用DBeaver访问，mysql 默认密码已经在.env中设置为`admin888`

![image-20200112130002715](./assets/image-20200112130002715.png)



## 代理转发

上面使用的是一个容器中存在多个项目，现在我们讨论的是每个项目是独立的容器。

### 容器配置

为每个项目设置配置项，如果端口或容器名相同会造成冲突无法启动，所以要为不同项目配置独立的端口



**houdunren/.env**

```
#容器名前缀
CONTAINER_NAME_PRE=hdcms

#宿主机映射到容器的端口
PORT=8081

#宿主机映射到MYSQL容器的端口
MYSQL_PORT=33061
```

**hdcms/.env**

```
#容器名前缀
CONTAINER_NAME_PRE=hdcms

#宿主机映射到容器的端口
PORT=8082

#宿主机映射到MYSQL容器的端口j
MYSQL_PORT=33062
```



### 执行编译

在两个项目中分别执行命令完成编译

```
docker-compose up -d
```

现在可以看到容器列表

```
docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Ports}}"
```

![image-20200112143444261](./assets/image-20200112143444261.png)



### 宿主配置

在宿主环境中安装NGINX服务器软件，NGINX在不同系统中的配置文件存放目录不同

#### MAC

下面安装 NGINX 服务器软件

```
brew install nginx
```

调试中难免有错误发生，通过错误日志可以很好排查

```
cat /usr/local/var/log/nginx/error.log
```

修改配置文件后需要重起NGINX，下面介绍基本的管理命令

```
#开启
brew services start nginx
#停止
brew services stop nginx
#重起
brew services restart nginx
```

**文件说明**

1. `/usr/local/etc/nginx/nginx.conf` 为主配置文件
2. `/usr/local/etc/servers`  目录存放自定义配置文件，这样就不需要改主配置文件了



#### CENTOS

下面安装 NGINX 服务器软件

```
yum update && yum install nginx -y
```

调试中难免有错误发生，通过错误日志可以很好排查

```
cat /usr/local/var/log/nginx/error.log
```

修改配置文件后需要重起NGINX，下面介绍基本的管理命令

```
#开启
systemctl start nginx
#停止
systemctl stop nginx
#重起
systemctl restart nginx
```

**文件说明**

1. `/etc/nginx/nginx.conf` 为主配置文件
2. `/etc/nginx/conf.d`  目录存放自定义配置文件，这样就不需要改主配置文件了



#### HOSTS

如果在本地测试时，修改 `/etc/hosts`文件中添加两个域名，线上使用将域名解析过来就可以 了

```
127.0.0.1	hdcms.test
127.0.0.1	houdunren.test
```



### 配置代理

修改主配置文件 `nginx.conf`将NGINX默认8080端口改变80

```
...
server {
	...
  listen       80;
  server_name  localhost;
  ...
}
```

在NGINX配置目录（centos为`/etc/nginx/conf.d`）创建 **hdcms.conf** ， **houdunren.conf** 两个配置文件

**houdunren.conf** 

```
server {
    listen       80;
    server_name houdunren.test;
    access_log  /var/log/nginx/houdunren.log;
    error_log /var/log/nginx/houdunren.error.log;
    location / {
    		proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8081;
    }
}
```

**hdcms.conf** 

```
server {
    listen       80;
    server_name hdcms.test;
    access_log  /var/log/nginx/hdcms.log;
    error_log /var/log/nginx/hdcms.error.log; 
    location / {
    		proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8082;
    }
}
```

现在使用不同域名访问就可以被代理到不同的容器中了



